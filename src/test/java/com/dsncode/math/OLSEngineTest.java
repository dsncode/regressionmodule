package com.dsncode.math;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class OLSEngineTest {

    @Test
    public void TestOLSBasicInput(){
        //based on example on https://www.codeproject.com/Articles/566326/Multi-Linear-Regression-in-Java
        /*  _____________________________
            Diet score	Male	age>20	| BMI
            =================================
            4	        0	    1	    | 27
            7	        1	    1	    | 29
            6	        1	    0	    | 23
            2	        0	    0	    | 20
            3	        0	    1	    | 21
         */

        OLSMultipleRegresionEngine engine = new OLSMultipleRegresionEngine(3);

        double[] sample = new double[]{4,0,1};
        engine.addSample(27d,sample);

        sample = new double[]{7,1,1};
        engine.addSample(29d,sample);

        sample = new double[]{6,1,0};
        engine.addSample(23d,sample);

        sample = new double[]{2,0,0};
        engine.addSample(20d,sample);

        sample = new double[]{3,0,1};
        engine.addSample(21d,sample);

        engine.compute();

        // predict
        sample = new double[]{4,0,1};
        double prediction = engine.predict(sample);
        double expected = 27d;

        assertEquals(expected,prediction,0.01d);

    }

    @Test // Last input on array is a dependent variable
    public void TestOLSSingleArrayInput(){
        //based on example on https://www.codeproject.com/Articles/566326/Multi-Linear-Regression-in-Java
        /*  _____________________________
            Diet score	Male	age>20	| BMI
            =================================
            4	        0	    1	    | 27
            7	        1	    1	    | 29
            6	        1	    0	    | 23
            2	        0	    0	    | 20
            3	        0	    1	    | 21
         */

        OLSMultipleRegresionEngine engine = new OLSMultipleRegresionEngine(3);

        double[] sample = new double[]{4,0,1,27d};
        engine.addSample(sample);

        sample = new double[]{7,1,1,29d};
        engine.addSample(sample);

        sample = new double[]{6,1,0,23d};
        engine.addSample(sample);

        sample = new double[]{2,0,0,20d};
        engine.addSample(sample);

        sample = new double[]{3,0,1,21d};
        engine.addSample(sample);

        engine.compute();

        // predict
        sample = new double[]{4,0,1};
        double prediction = engine.predict(sample);
        double expected = 27d;

        assertEquals(expected,prediction,0.01d);

    }

}
