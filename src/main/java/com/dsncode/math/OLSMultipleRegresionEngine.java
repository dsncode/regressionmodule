package com.dsncode.math;

import org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class OLSMultipleRegresionEngine {

    private OLSMultipleLinearRegression regression = new OLSMultipleLinearRegression();
    private int independentVariableLength = 0;
    private List<Double> dependentVariable = new LinkedList<Double>();
    private List<double[]> independentVariables = new LinkedList<double[]>();

    public OLSMultipleRegresionEngine(int independentVariableSize){
        this.independentVariableLength =independentVariableSize;
    }


    public void addSample(Double result, double[] obs){
        dataValidation(obs,0);
        dependentVariable.add(result);
        independentVariables.add(obs);
    }

    // last value is the observed result
    public void addSample(double[] obs){
        dataValidation(obs,1);
        dependentVariable.add(obs[obs.length-1]);
        independentVariables.add(java.util.Arrays.copyOfRange(obs,0,obs.length-1));
    }

    private double[] toPrimitive(List<Double> arr){
        double[] target = new double[arr.size()];
        for(int i = 0;i<arr.size();i++){
            target[i] = arr.get(i);
        }
        return target;
    }

    private double[] toPrimitive(Double[] arr){
        double[] target = new double[arr.length];
        int index = 0;
        for(double value : arr){
            target[index] = value;
        }
        return target;
    }

    public void compute(){

        double[] y = toPrimitive(this.dependentVariable);

        double[][] x = new double[independentVariables.size()][this.independentVariableLength];

        int index = 0;
        for(double[] data : this.independentVariables){
            x[index] = data;
            index++;
        }

        this.independentVariables.clear();
        this.dependentVariable.clear();

        regression.newSampleData(y,x);

    }

    public double[] getRegresionParam(){
        return regression.estimateRegressionParameters();
    }

    public double predict(double[] obs){
        dataValidation(obs,0);

        double[] param = regression.estimateRegressionParameters();

        double tot = param[0];
        int index = 1;
        for(double input : obs){
            tot+= input * param[index];
            index++;
        }

        return tot;
    }

    public void dataValidation(double[] obs, int delta){
        if( (obs.length-delta) != this.independentVariableLength){
            throw new IllegalArgumentException("wrong sample size");
        }
    }

}
